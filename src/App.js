import React, { useState, useEffect } from "react";
import "./App.css";

function App() {
  const [items, setItem] = useState({});
  const [size, setSize] = useState("50vh");
  const [displayExplanation, setDisplay] = useState("hidden");

  useEffect(() => {
    getItems();
    document.title = `You clicked ${size} times`;
  }, []);

  useEffect(() => {
    document.title = `L'image fait ${size}`;
  });

  const getItems = async () => {
    const response = await fetch(
      "https://api.nasa.gov/planetary/apod?api_key=6hraapVjx8aQU67DpFjUsqUy4ASDghkF5rphLpJo"
    );
    const data = await response.json();
    setItem(data);
    console.log(data);
  };

  const updateSize = () => {
    if (size == "50vh") {
      setSize(["100vh"]);
    } else {
      setSize(["50vh"]);
    }
    console.log(size);
  };

  const flavorText = () => {
    if (size == "50vh") {
      return <p>Cliquez sur l'image pour l'agrandir</p>;
    } else {
      return <p>Cliquez sur l'image pour la réduire</p>;
    }
  };

  const explanationClass = () => {
    if (displayExplanation == "hidden") {
      setDisplay("explanation");
    } else {
      setDisplay("hidden");
    }
  };

  return (
    <div className="App">
      <h1>Nasa API</h1>
      <p>{items.title}</p>
      <img
        src={items.url}
        alt={items.explanation}
        style={{ width: size }}
        onClick={updateSize}
      />
      {flavorText()}
      <button onClick={explanationClass}>Plus d'infos</button>
      <div className={displayExplanation}>
        <p>{items.explanation}</p>
      </div>
    </div>
  );
}

export default App;
